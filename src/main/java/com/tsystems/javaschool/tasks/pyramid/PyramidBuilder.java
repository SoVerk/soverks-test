package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int check = inputNumbers.size();
        int rows=0;
            for (int i=1;i<=inputNumbers.size();i++) {
                check -= i;
                Object val = inputNumbers.get(i);
                if (check == 0) {
                    rows = i;
                    break;
                } else if (check < 0||val==null) {
                    throw new CannotBuildPyramidException();
                }
            }
            for (int i=0;i<inputNumbers.size();i++){
                Object val = inputNumbers.get(i);
                if(val==null){
                    throw new CannotBuildPyramidException();
                }
            }

        inputNumbers = sortNumbers(inputNumbers);
        int cols = (rows*2)-1;
        int[][]arr = new int[rows][cols];
        int item=0;
        for (int i=0;i<rows;i++){
            int pos = (cols-rows)-i;
            int step=0;
            int counter=0;
            for(int j=0;j<cols;j++){
                if (j==pos||counter<i+1&&j==pos+step){
                    arr[i][j]=inputNumbers.get(item);
                    item++;
                    counter++;
                    step+=2;
                } else {
                    arr[i][j]=0;
                }
                System.out.print(arr[i][j]+" ");
            }
            System.out.println();
        }
        return arr;
    }

    public List<Integer> sortNumbers(List<Integer> unsortNumbers) {
        int temp;
        int counter=1;
        for(;counter!=0;){
            counter = 0;
            for(int i=0;i<unsortNumbers.size()-1;i++) {
                if(unsortNumbers.get(i)>unsortNumbers.get(i+1)){
                    temp=unsortNumbers.get(i+1);
                    unsortNumbers.set(i+1, unsortNumbers.get(i));
                    unsortNumbers.set(i, temp);
                    counter++;
                }else if(counter==0&&i==unsortNumbers.size()-1){
                    break;
                }
            }
        }
        System.out.println(unsortNumbers);
        return unsortNumbers;
    }

}
