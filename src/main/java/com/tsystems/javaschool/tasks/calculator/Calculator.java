package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import org.jetbrains.annotations.Nullable;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public @Nullable String evaluate(@Nullable String statement) {
        String number = statement;
        if (number==null){
            return null;
        }
        String type = "int";
        String[] arr = number.split("(?<=\\d)(?=\\D)|(?<=\\D)(?=\\d)|(?<=\\D)(?=\\D)");
        ArrayList<String> arrayList =
                new ArrayList<String>(Arrays.asList(arr));
        for(int i=0;i<arrayList.size();i++){
            if(arrayList.get(i).equals(".")){
                String temp = arrayList.get(i-1)+arrayList.get(i)+arrayList.get(i+1);
                arrayList.remove(i+1);
                arrayList.remove(i-1);
                arrayList.set(i-1, temp);
                type = "db";
            }
        }
        String result = calc(arrayList, type);
        boolean integer = true;
        int start=0;
        int end=0;
        if (!result.equals("null")){
            for(int i=0;i<result.length();i++){
                char temp = result.charAt(i);
                if(temp=='.'){
                    end = i+(result.length()-i);
                    if (result.length()-i>5){
                        System.out.println("bloo");
                        end=i+5;
                    }
                    for(int j=i+1;j<result.length();j++){
                        temp = result.charAt(j);
                        if (temp!='0'){
                            integer = false;
                            break;
                        }
                    }
                }
            }
            if (integer==true){
                result = result.replaceAll("\\..*", "");
            } else {
                result = result.substring(start,end);
            }
        }else if (result.equals("null")){
            result = null;
            return result;
        }
           return result;
    }

    public static @Nullable String calc(@Nullable ArrayList<String> arr, String type){
        if(arr.contains("")||arr.contains("null")){
            return "null";
        }
        String result="";
        String tp = type;
        int loop=0;
        arr = new ArrayList<String>(arr);
        for(int i=0;;i++){
            if(loop>5){
                return result;
            }else if(arr.size()==1){
                return arr.get(0);
            }
            if(arr.get(i).equals("(")&&i<arr.size()-1){
                ArrayList<String> uni = new ArrayList<String>();
                int counter=0;
                int j=i;
                do{
                    uni.add(counter, arr.get(j));
                    arr.remove(j);
                    counter++;
                    if(j==arr.size()-1){
                        if (arr.get(j).equals(")")){
                            uni.add(counter, arr.get(j));
                            arr.remove(j);
                            break;
                        } else{
                            return "null";
                        }
                    }else if(arr.get(j).equals(")")){
                        uni.add(counter, arr.get(j));
                        arr.remove(j);
                        break;
                    }
                } while (true);
                uni.remove(0);
                uni.remove(uni.size()-1);
                String res=calc(uni, tp);
                arr.add(i, res);
            }

            if(i==arr.size()-1){
                i=0;
                loop++;
            }else if(arr.size()==3){
                result = calc(arr.get(0),arr.get(1),arr.get(2), tp);
                break;
            }
            if (arr.get(i).equals("/")&&loop>2||arr.get(i).equals("*")&&loop>2){
            } else if (arr.get(i).equals("+")&&loop>3||arr.get(i).equals("-")&&loop>3){
            } else {
                continue;
            }
            String res = calc(arr.get(i-1),arr.get(i),arr.get(i+1), tp);
            if (res.equals("null")){
                return res;
            }
            arr.remove(i+1);
            arr.remove(i-1);
            arr.set(i-1, res);
            i=0;
        }
        if (result.equals("")){
            return "null";
        }
        return result;
    }

    private static @Nullable String calc(@Nullable String get,@Nullable String get0,@Nullable String get1, String tp) {
        String result = "";
        String type = tp;
        Double temp;
        Double a;
        Double b;
        try{
            a = Double.valueOf(get);
            b = Double.valueOf(get1);
            if(a.isNaN()||b.isNaN()){
                return "null";
            }
            if (get0.equals("*")){
                temp=a*b;
            }else if(get0.equals("/")){
                temp=a/b;
            }else if(get0.equals("+")){
                temp=a+b;
            }else if(get0.equals("-")){
                temp=a-b;
            } else{
                System.out.println("Calculate error");
                return "null";
            }
        } catch (Exception ex){
            System.out.println("Calculate error");
            return "null";
        }
        boolean check = Double.isFinite(temp);

        if(!check){
            System.out.println("Infinite value");
            return "null";
        }else if (temp.isNaN()){
            System.out.println("NULL spotted");
            return "null";
        }
        result = Double.toString(temp);
        return result;
    }
}
