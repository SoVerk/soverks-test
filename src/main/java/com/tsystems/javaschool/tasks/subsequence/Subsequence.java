package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x==null || y==null) {
            throw new IllegalArgumentException();
        }else if (x.size()==y.size()&&x.size()==0){
            boolean result = x.equals(y);
            return result;
        } else if (x.isEmpty()){
            return true;
        }
        boolean result = false;
        int point = 0;
        int num = 0;
        for (int i=0;i<x.size();i++){
            for (int j=point;j<y.size();j++){
                Object a = x.get(i);
                Object b = y.get(j);
                if (a==b||a.equals(b)){
                    point = j;
                    num++;
                    continue;
                }
            }
            if (num == x.size()) {
                result = true;
                return result;
            }
        }
        return result;
    }

}